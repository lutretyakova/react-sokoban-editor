# Редактор уровней игры "Сокобан"
![Игра "Сокобан"](img/sokobaneditor/sokoban2.png)

[Ссылка на репозиторий Редактора уровней](https://gitlab.com/lutretyakova/react-sokoban-editor)

[Сделать свой уровень](https://lutretyakova.gitlab.io/projects/react-sokoban-editor)

[Ссылка на репозиторий игрушки](https://gitlab.com/lutretyakova/react-sokoban-with-levels)

[Поиграть](https://lutretyakova.gitlab.io/projects/react-sokoban-with-levels)