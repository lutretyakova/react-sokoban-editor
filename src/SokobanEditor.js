import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from './actions';

import './SokobanEditor.css';

import Editor from './modules/Editor';

class App extends Component {
    render() {
        return (
            <div className="page">
                <div className="page-header">
                    <header>
                        <h1>Sokoban<span>Editor</span></h1>
                    </header>
                </div>
                <Editor />
                <footer>
                    <a href="https://gitlab.com/lutretyakova/react-sokoban-editor">
                        https://gitlab.com/lutretyakova/react-sokoban-editor
                    </a>
                </footer>
            </div>
        );
    }
}

export default connect(state => ({state}), actions)(App);
