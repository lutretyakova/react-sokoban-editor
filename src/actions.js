export const CREATE_MAP = 'createMap';

export const SAVE_BOARD = 'saveBoard';

export const SET_SAVE_FLAG = 'setSaveFlag';

export const EDIT_LEVEL = 'editLevel';

export const RESET_SAVE_FLAG = 'resetSaveFlag';

export const createNewMap = () => ({
    type: CREATE_MAP
});

export const saveBoard = (board, isHumanSet) => ({
    type: SAVE_BOARD,
    board,
    isHumanSet
});

export const setSaveFlag = () => ({
    type: SET_SAVE_FLAG
});

export const editLevel = (idLevel, board) => ({
    type: EDIT_LEVEL,
    idLevel,
    board
});

export const resetSaveFlag = () => ({
    type: RESET_SAVE_FLAG
})
