import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import reducer from './reducer';
import SokobanEditor from './SokobanEditor';
import './index.css';

const store = createStore(reducer);

ReactDOM.render(
    <Provider store={store}>
        <SokobanEditor />
    </Provider>, 
    document.getElementById('root'));
registerServiceWorker();
