import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions';

class CreateMapButton extends Component{
    render(){
        return(
            <div onClick={() => this.handleClick()} className="btn">
                <i className="fas fa-plus"></i>&nbsp;Создать карту 
            </div>
        )
    }


    handleClick(){
        if ( !this.props.state.isShowCreateModal)
            {
                const { createNewMap } = this.props;
                createNewMap();
            }
    }
}

export default connect((state) => ({state}), actions)(CreateMapButton)