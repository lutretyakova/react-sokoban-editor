import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

import GameBoard from './GameBoard';
import ListPanel from './ListPanel';
import { INIT } from './constants';

class Editor extends Component {
    render() {
        const { state } = this.props;
        const gameBoard = state.boardState !== INIT
            ? <GameBoard key={state.key} />
            : null;
        return (
            <div className="container" ref="container">
                { gameBoard }
                <ListPanel />
            </div>
        )
    }
}

export default connect((state) => ({ state }), actions)(Editor);