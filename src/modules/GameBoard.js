import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

import empty from '../images/empty.png';
import trash from '../images/trash.png';
import { loadImage } from './files';
import { TOOL_IMAGES } from './files';
import {
    LEFT_MOUSE_BUTTON, CELL_SIZE, WIDTH, HEIGHT,
    EMPTY,
    DELETE_KEY_CODE, SAVE_FLAG, HUMAN
} from './constants';

const OFFSET_TOP = 16;
const BUTTON_STEP = 10;
const BUTTON_SIZE = Math.ceil(CELL_SIZE * 1.5);
const BUTTON_COUNT = 4;
const TOOL_PANEL_WIDTH = 2 * BUTTON_SIZE;
const BUTTON_LEFT_POS = Math.ceil((TOOL_PANEL_WIDTH - BUTTON_SIZE) / 2);

const BOARD_BACK_ICON = loadImage(empty);
const BOARD_WIDTH = WIDTH * CELL_SIZE;
const BOARD_HEIGHT = HEIGHT * CELL_SIZE;

const GRID_COLOR = "#c4a64e";
const TOOL_PANEL_COLOR = '#555';
const SELECT_COLOR = 'tomato';
const DRAG_COLOR = 'wheat';

const ADDITIONAL_PANEL_WIDTH = Math.ceil(CELL_SIZE * 1.2);
const TRASH_ICON = loadImage(trash);

const LIST_PANEL_WIDTH = 16 * 16; //16 em
const MIN_WINDOW_WIDTH = TOOL_PANEL_WIDTH + ADDITIONAL_PANEL_WIDTH + WIDTH * CELL_SIZE;
const HEADER_FOOTER_HEIGHT = 7 * 16;
const HEADER_HEIGHT = 4 * 16;

class GameBoard extends Component {
    getGridCoord(y, x) {
        const posY = Math.floor((y - OFFSET_TOP) / CELL_SIZE);
        const posX = Math.floor((x - this.state.boardLeftPos) / CELL_SIZE);
        return [posY, posX]
    }

    isInsideBoard(y, x) {
        const boardMaxX = this.state.boardLeftPos + BOARD_WIDTH;
        const boardMaxY = OFFSET_TOP + BOARD_HEIGHT;
        return (x >= this.state.boardLeftPos && x <= boardMaxX &&
            y >= OFFSET_TOP && y <= boardMaxY)
    }

    constructor(props) {
        super(props);

        const { width, height, boardLeftPos, trashLeftPos } = this.defineBoardCoords();
        let board = this.props.state.board.map(row => row.slice());
        this.isHumanSet = board.join().search(HUMAN) === -1
            ? false
            : true;

        this.state = {
            board,
            width,
            height,
            boardLeftPos,
            trashLeftPos,
            block: null,
            selected: false,
            isDown: false,
            isMoving: false,
        }
        this.defineBoardCoords()
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.handleResize = this.handleResize.bind(this);
    }

    defineBoardCoords() {
        const width = Math.max(
            document.getElementsByClassName("container")[0].clientWidth - LIST_PANEL_WIDTH,
            MIN_WINDOW_WIDTH
        );
        const height = window.innerHeight - HEADER_FOOTER_HEIGHT;

        const boardLeftPos = Math.max(
            Math.ceil(width - TOOL_PANEL_WIDTH - BOARD_WIDTH - ADDITIONAL_PANEL_WIDTH) / 2 + TOOL_PANEL_WIDTH,
            TOOL_PANEL_WIDTH
        );

        const trashLeftPos = width - ADDITIONAL_PANEL_WIDTH + (ADDITIONAL_PANEL_WIDTH - CELL_SIZE) / 2;

        return {
            width,
            height,
            boardLeftPos,
            trashLeftPos
        }
    }

    render() {
        const coverStyle = {
            top: `${HEADER_HEIGHT}px`,
            minWidth: `${MIN_WINDOW_WIDTH}px`
        }
        return (
            <div ref="coverBlock" className="cover-block" style={{ minWidth: `${MIN_WINDOW_WIDTH}px` }}>
                <canvas ref="cover" id="cover" style={coverStyle} width={this.state.width} height={this.state.height}
                    onMouseDown={(event) => this.handleMouseDown(event)}
                    onMouseMove={(event) => this.handleMouseMove(event)}
                    onMouseUp={(event) => this.handleMouseUp(event)}>
                </canvas>
            </div>
        )
    }

    componentDidMount() {
        window.addEventListener("keydown", this.handleKeyDown);
        window.addEventListener("resize", this.handleResize);
        this.updateCover();
    }

    componentDidUpdate() {
        this.updateCover();

        if (this.props.state.saveState === SAVE_FLAG) {
            const { saveBoard } = this.props;
            saveBoard(this.state.board, this.isHumanSet);
        }
    }

    updateCover() {
        const ctx = this.refs.cover.getContext('2d');
        ctx.clearRect(0, 0, this.state.width, this.state.height);

        this.drawToolPanel(ctx);
        this.drawAdditionalPanel(ctx);
        this.drawEmptyGameBoard(ctx);
        this.drawGameObjectOnBoard(ctx);

        if (this.state.block) {
            if (this.state.isDown && this.state.isMoving) {
                this.drawDraggingBlock(ctx);
            } else if (this.state.selected) {
                this.drawSelectedBlock(ctx);
            }
        }
    }

    drawToolPanel(ctx) {
        ctx.fillStyle = TOOL_PANEL_COLOR;
        ctx.fillRect(0, 0, TOOL_PANEL_WIDTH, this.state.height);

        Object.keys(TOOL_IMAGES).forEach((key, inx) => {
            TOOL_IMAGES[key].then((img) => {
                const y = inx * (BUTTON_SIZE + BUTTON_STEP) + OFFSET_TOP;
                ctx.globalAlpha = key === HUMAN.toString() && this.isHumanSet ? 0.4: 1;
                ctx.drawImage(img, BUTTON_LEFT_POS, y, BUTTON_SIZE, BUTTON_SIZE)
            })
        });
    }

    drawAdditionalPanel(ctx) {
        const x = this.state.width - ADDITIONAL_PANEL_WIDTH;
        ctx.fillRect(x, 0, ADDITIONAL_PANEL_WIDTH, this.state.height);
        if (this.state.selected) {
            TRASH_ICON.then(img => {
                ctx.drawImage(img, this.state.trashLeftPos, OFFSET_TOP, CELL_SIZE, CELL_SIZE);
            })
        }
    }

    drawEmptyGameBoard(ctx) {
        BOARD_BACK_ICON.then(img => {
            let ptrn = ctx.createPattern(img, 'repeat');
            ctx.fillStyle = ptrn;
            ctx.fillRect(this.state.boardLeftPos, OFFSET_TOP, BOARD_WIDTH, BOARD_HEIGHT);

            // grid game board
            ctx.strokeStyle = GRID_COLOR;
            for (let y = CELL_SIZE; y < BOARD_HEIGHT; y += CELL_SIZE) {
                ctx.beginPath();
                ctx.moveTo(this.state.boardLeftPos, y + OFFSET_TOP);
                ctx.lineTo(this.state.boardLeftPos + BOARD_WIDTH, y + OFFSET_TOP);
                ctx.stroke();
            }

            for (let x = CELL_SIZE; x < BOARD_WIDTH; x += CELL_SIZE) {
                ctx.beginPath();
                ctx.moveTo(x + this.state.boardLeftPos, OFFSET_TOP);
                ctx.lineTo(x + this.state.boardLeftPos, BOARD_HEIGHT + OFFSET_TOP);
                ctx.stroke();
            }
        });
    }

    drawGameObjectOnBoard(ctx) {
        for (let y = 0; y < HEIGHT; y++) {
            for (let x = 0; x < WIDTH; x++) {
                if (this.state.board[y][x] !== EMPTY) {
                    TOOL_IMAGES[this.state.board[y][x]].then(img => {
                        const dx = this.state.boardLeftPos + x * CELL_SIZE;
                        const dy = y * CELL_SIZE + OFFSET_TOP;
                        ctx.drawImage(img, dx, dy, CELL_SIZE, CELL_SIZE)
                    })
                }
            }
        }
    }

    drawDraggingBlock(ctx) {
        TOOL_IMAGES[this.state.block].then(img => {
            let x, y;
            if (this.isInsideBoard(this.state.yPos, this.state.xPos)) {
                const [cellY, cellX] = this.getGridCoord(this.state.yPos, this.state.xPos);
                x = this.state.boardLeftPos + cellX * CELL_SIZE;
                y = cellY * CELL_SIZE + OFFSET_TOP;
            } else {
                x = this.state.xPos - Math.ceil(CELL_SIZE / 2);
                y = this.state.yPos - Math.ceil(CELL_SIZE / 2);
            }
            ctx.drawImage(img, x, y, CELL_SIZE, CELL_SIZE);
            ctx.strokeStyle = DRAG_COLOR;
            ctx.strokeRect(x, y, CELL_SIZE, CELL_SIZE);
        })
    }

    drawSelectedBlock(ctx) {
        TOOL_IMAGES[this.state.block].then(img => {
            let x, y;
            const [cellY, cellX] = this.getGridCoord(this.state.yPos, this.state.xPos);
            x = this.state.boardLeftPos + cellX * CELL_SIZE;
            y = cellY * CELL_SIZE + OFFSET_TOP;
            ctx.drawImage(img, x, y, CELL_SIZE, CELL_SIZE);

            ctx.strokeStyle = SELECT_COLOR;
            ctx.strokeRect(x, y, CELL_SIZE, CELL_SIZE);
        })
    }

    handleMouseDown(event) {
        if (event.buttons === LEFT_MOUSE_BUTTON) {
            const xPos = event.pageX;
            const yPos = event.pageY - HEADER_HEIGHT;
            if (!this.state.selected) {
                let block = this.state.block;
                let board = this.state.board.map(row => row.slice());

                const btnMaxY = OFFSET_TOP + BUTTON_COUNT * BUTTON_SIZE + (BUTTON_COUNT - 1) * BUTTON_STEP;
                const btnMaxX = BUTTON_LEFT_POS + BUTTON_SIZE;

                if (xPos >= BUTTON_LEFT_POS && xPos <= btnMaxX
                    && yPos >= OFFSET_TOP && yPos <= btnMaxY
                ) {
                    Object.keys(TOOL_IMAGES).forEach((key, inx) => {
                        const y = inx * (BUTTON_SIZE + BUTTON_STEP) + OFFSET_TOP;
                        if (yPos > y && yPos < y + BUTTON_SIZE) {
                            block = key === HUMAN.toString() && this.isHumanSet ? null: key;
                        }
                    })
                } else if (this.isInsideBoard(yPos, xPos)) {
                    const [cellY, cellX] = this.getGridCoord(yPos, xPos);
                    if (this.state.board[cellY][cellX] !== EMPTY) {

                        board[cellY][cellX] = EMPTY;
                        block = this.state.board[cellY][cellX];
                    }
                }

                this.setState({
                    xPos,
                    yPos,
                    block,
                    board,
                    isDown: true
                })
            } else {
                if (xPos >= this.state.trashLeftPos && xPos <= this.state.trashLeftPos + CELL_SIZE &&
                    yPos >= OFFSET_TOP && yPos <= OFFSET_TOP + CELL_SIZE) {
                    // trashIcon cLick
                    this.isHumanSet = this.isHumanSet && this.state.block === HUMAN.toString()
                        ? false
                        : true
                    this.setState({
                        selected: false,
                        isMoving: false,
                        isDown: false,
                        block: null
                    })
                }
            }
        }
    }

    handleMouseMove(event) {
        if (event.buttons === LEFT_MOUSE_BUTTON && !this.state.selected) {
            this.setState({
                xPos: event.pageX,
                yPos: event.pageY - HEADER_HEIGHT,
                isMoving: true
            });
        }
    }

    handleMouseUp(event) {
        const x = event.pageX;
        const y = event.pageY - HEADER_HEIGHT;
        if (!this.state.isMoving && this.isInsideBoard(y, x) && this.state.block !== null) {
            const [cellY, cellX] = this.getGridCoord(this.state.yPos, this.state.xPos);
            const [oldCellY, oldCellX] = this.getGridCoord(y, x);
            if (this.state.selected) {
                if (cellY === oldCellY && cellX === oldCellX) {
                    let board = this.state.board.map(row => row.slice());
                    board[cellY][cellX] = this.state.block;

                    this.setState({
                        selected: false,
                        isMoving: false,
                        isDown: false,
                        block: null,
                        board
                    })
                }
            } else {
                this.setState({
                    selected: true,
                    isMoving: false,
                    isDown: false,
                    yPos: y,
                    xPos: x,
                })
            }
        } else if ((this.state.isDown && this.state.isMoving) && !this.state.selected) {
            if (this.isInsideBoard(y, x) && this.state.block !== null) {
                const [posY, posX] = this.getGridCoord(y, x);
                let board = this.state.board.map(row => row.slice());
                board[posY][posX] = this.state.block;
                
                this.isHumanSet = this.state.block === HUMAN.toString()
                    ? true
                    : this.isHumanSet 
                        ? true 
                        : false;

                this.setState({
                    board
                });
            }
            this.setState({
                isDown: false,
                isMoving: false,
            })
        }
    }

    handleKeyDown(event) {
        if (event.keyCode === DELETE_KEY_CODE && this.state.selected && this.state.block !== null) {
            this.isHumanSet = this.isHumanSet && this.state.block === HUMAN.toString()
                ? false
                : true
            this.setState({
                selected: false,
                isMoving: false,
                isDown: false,
                block: null
            })
        }
    }

    handleResize() {
        const { width, height, boardLeftPos, trashLeftPos } = this.defineBoardCoords();
        this.setState({
            width,
            height,
            boardLeftPos,
            trashLeftPos
        })
    }

    componentWillUnmount() {
        window.removeEventListener("keydown", this.handleKeyDown);
        window.removeEventListener("resize", this.handleResize);
    }

}

export default connect((state) => ({ state }), actions)(GameBoard);