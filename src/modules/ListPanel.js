import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions';

import {
    Stitch, RemoteMongoClient, AnonymousCredential
} from "mongodb-stitch-browser-sdk";

import CreateMapButton from './CreateMapButton';
import SaveButton from './SaveButton';
import { EDITED, CREATED, READY_TO_SAVE, WIDTH, HEIGHT, INIT } from './constants';

const SAVE_STARTED = 'Запись данных...';
const SAVE_ERROR = 'Произошла ошибка';

const client = Stitch.initializeDefaultAppClient('sokoban-ymorc');
const db = client.getServiceClient(RemoteMongoClient.factory, 'mongodb-atlas').db('sokoban');

class ListPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            active: -1,
            saveMsg: ""
        }
    }

    render() {
        const { state } = this.props;
        const levels = this.state.loaded
            ? Object.keys(this.state.levels).map((inxKey) => {
                let date = this.state.levels[inxKey].date !== undefined
                    ? new Date(this.state.levels[inxKey].date).toLocaleString('ru-ru')
                    : "???";
                const active = this.state.active === inxKey
                    ? "active"
                    : "";
                return <div className={`btn ${active}`} key={inxKey} id={inxKey} onClick={(e) => this.handleClick(e)}>
                    Уровень : {date}
                </div>
            })
            : null;

        let currentLevel = null;
        if (this.state.loaded && this.state.active !== -1 && state.boardState === EDITED) {
            const currentLevelDate = this.state.levels[this.state.active].date !== undefined
                ? new Date(this.state.levels[this.state.active].date).toLocaleString('ru-ru')
                : "???";
            currentLevel = <p>Текущий уровень от {currentLevelDate}</p>
        } else if (state.boardState === CREATED) {
            currentLevel = <p>Новый уровень</p>
        }

        const saveMsg = this.state.saveMsg !== ""
            ? <p>{this.state.saveMsg}</p>
            : null;

        const saveButton = [EDITED, CREATED].includes(state.boardState)
            ? <SaveButton />
            : null;

        const pnlClass = state.boardState === INIT
            ? "list-pnl"
            : "list-pnl right"
        return (
            <div className={pnlClass} >
                <div className="level-list">
                    <CreateMapButton />
                    {levels}
                </div>
                {currentLevel}
                {saveMsg}
                {saveButton}
            </div>
        )
    }

    componentDidMount() {
        client.auth.loginWithCredential(new AnonymousCredential())
            .then(user =>
                db.collection('level').find({}).asArray()
            ).then(levels => {
                this.saveLevels(levels);
            }).catch(err => {
                console.log(err)
            });
    }


    saveLevels(levels) {
        let records = {};
        for (let inx = 0; inx < levels.length; inx++) {
            records[inx] = levels[inx]
        }
        this.setState({
            levels: records,
            loaded: true
        })
    }


    handleClick(event) {
        const { editLevel } = this.props;
        editLevel(event.target.id, this.state.levels[event.target.id].board);
        this.setState({
            active: event.target.id
        })
    }


    componentDidUpdate() {
        const { state } = this.props;
        if (state.saveState === READY_TO_SAVE) {
            this.setState({
                saveMsg: SAVE_STARTED
            })
            client.auth.loginWithCredential(new AnonymousCredential())
                .then(user => {
                    if (state.boardState === CREATED) {
                        db.collection('level').insertOne({
                            owner_id: client.auth.user.id,
                            date: new Date(),
                            width: WIDTH,
                            height: HEIGHT,
                            board: state.board,
                            isHumanSet: state.isHumanSet,
                        }).then((_) => this.updateList(Object.keys(this.state.levels).length.toString()));
                    } else if (state.boardState === EDITED) {
                        db.collection('level').updateOne(
                            { _id: this.state.levels[state.idLevel]._id },
                            {
                                $set: { 
                                    board: state.board,
                                    isHumanSet: state.isHumanSet
                                }
                            },
                            { upsert: true }
                        ).then((doc) => this.updateList(this.state.active))
                    }
                })
                .catch(err => {
                    this.setState({
                        saveMsg: SAVE_ERROR
                    })
                    console.log(err)
                });

            const { resetSaveFlag } = this.props;
            resetSaveFlag();
        }
    }


    updateList(activeLevel) {
        client.auth.loginWithCredential(new AnonymousCredential())
            .then(user =>
                db.collection('level').find({}).asArray()
            )
            .then(levels => {
                this.saveLevels(levels);
                const { editLevel } = this.props;
                editLevel(activeLevel, levels[activeLevel].board);

                this.setState({
                    active: activeLevel,
                    saveMsg: ""
                })
            })
            .catch(err => {
                console.log(err)
            });

    }
}

export default connect((state) => ({ state }), actions)(ListPanel)