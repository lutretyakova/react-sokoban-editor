import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions';

class SaveButton extends Component {
    render() {
        return(
            <div onClick = {() => this.handleClick()} className="btn">
                <i className="fas fa-save"></i>&nbsp;Сохранить
            </div>
        )
    }

    handleClick(){
        const {setSaveFlag} = this.props;
        setSaveFlag();
    }
}

export default connect((state) => ({state}), actions)(SaveButton)
