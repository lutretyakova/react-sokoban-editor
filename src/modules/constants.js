export const WIDTH = 18;

export const HEIGHT = 15;

export const CELL_SIZE = 42;

export const EMPTY = 0;

export const HUMAN = 1;

export const WALL = 2;

export const BOX = 3;

export const GOAL = 4;

export const LEFT_MOUSE_BUTTON = 1;

export const DELETE_KEY_CODE = 46;

export const EDITED = 'edited';

export const CREATED = 'created';

export const INIT = 'init';

export const SAVE_FLAG = 'saveFlagSet';

export const READY_TO_SAVE = 'readyToSave';