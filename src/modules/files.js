import human from '../images/hdown.png';
import box from '../images/box.png';
import goal from '../images/goal.png';
import wall from '../images/wall.png';
import { GOAL, HUMAN , WALL, BOX} from './constants';

export const loadImage = (url) => {
    return new Promise((resolve, reject) => {
        let img = new Image();
        img.addEventListener('load', e => resolve(img));
        img.addEventListener('error', () => {
            reject(new Error(`Failed to load image's URL: ${url}`));
        });
        img.src = url;
    });
}

export const TOOL_IMAGES = {
    [ WALL ]: loadImage(wall), 
    [ BOX ]: loadImage(box), 
    [ GOAL ]: loadImage(goal),
    [ HUMAN ]: loadImage(human)
};
