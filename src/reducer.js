import {
    CREATE_MAP, SAVE_BOARD, SET_SAVE_FLAG, EDIT_LEVEL, RESET_SAVE_FLAG
} from "./actions";

import {
    WIDTH, HEIGHT, CREATED, EDITED, EMPTY, INIT, SAVE_FLAG, READY_TO_SAVE
} from './modules/constants';


export default (state = init(), actions) => {
    switch (actions.type) {
        case CREATE_MAP:
            return createMap(state);
        case SET_SAVE_FLAG:
            return setSaveFlag(state);
        case SAVE_BOARD:
            return saveBoard(state, actions.board, actions.isHumanSet);
        case EDIT_LEVEL:
            return editLevel(state, actions.idLevel, actions.board);
        case RESET_SAVE_FLAG:
            return resetSaveFlag(state);
        default:
            return state
    }
}

const init = () => {
    return {
        key: 1,
        boardState: INIT,
        saveState: INIT
    }
}

const createMap = (state) => {
    let board = (new Array(HEIGHT)).fill(null);
    board = board.map((_) => ((new Array(WIDTH)).fill(EMPTY)));
    return {
        ...state,
        board,
        boardState: CREATED,
        key: state.key + 1
    }
}

const setSaveFlag = (state) => {
    return {
        ...state,
        saveState: SAVE_FLAG,
    }
}

const saveBoard = (state, board, isHumanSet) => {
    let boardStr = board.map(row => row.slice()).join();
    return {
        ...state,
        saveState: READY_TO_SAVE,
        board: boardStr,
        isHumanSet
    }
}

const editLevel = (state, idLevel, editBoard) => {
    let mapLine = editBoard
        .split(',')
        .map(elem => parseInt(elem, 10));

    let board = (new Array(HEIGHT)).fill(null)
        .map((_, inx) => (
            mapLine.slice(inx * WIDTH, (inx + 1) * WIDTH)
        ));
    return {
        ...state,
        idLevel,
        boardState: EDITED,
        board,
        key: state.key + 1
    }
}

const resetSaveFlag = (state) => {
    return {
        ...state,
        saveState: INIT
    }
}